//获取后端数据 axios


//展示后端数据 vue


var aaa = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        spit: null,
        comments: [],
        content: '',
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        spit_id: ''
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        this.spit_id = this.get_query_string('spit_id')
        // 一加载就需要做的，直接是代码
        axios.get(HOST + "/spits/" + get_query_string('spit_id', 1))
            .then(response => {
                //console.log(response)
                this.spit = response.data
            })
            .catch(error => {
            }),

            // 一加载就需要做的，直接是代码
            axios.get(HOST + "/comments/" + get_query_string("spit_id", 1))
                .then(response => {
                    //console.log(response)
                    this.comments = response.data
                })
                .catch(error => {
                })
    },
    methods: {
        get_query_string: function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        commentsubmit: function () {
            axios.post(HOST + '/commentsubmit/', {
                user: this.user_id,
                spits: this.spit_id,
                content: this.content,
            }, {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
                withCredentials: true
            })
                .then(response => {
                    // 记录用户的登录状态
                    alert('评论成功')
                    location.reload()
                })
                .catch(error => {
                    if (error.response.status == 400) {
                        if ('non_field_errors' in error.response.data) {
                            this.error_sms_code_message = error.response.data.non_field_errors[0];
                        } else {
                            this.error_sms_code_message = '数据有误';
                        }
                        this.error_sms_code = true;
                    } else {
                        console.log(error.response.data);
                    }
                })
        },
        tog_like: function () {
            var like = this.spit.like += 1
            axios.put(HOST + '/spitlike/' + this.spit.id + '/' , {
                user: this.user_id,
                like: like,
            }, {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
                withCredentials: true
            })
                .then(response => {
                    // this.question = response.data;
                })
                .catch(response => {
                    console.log(error.response.data)
                })
        },
        toggle_like: function (comment) {
            var like = comment.like += 1
            axios.put(HOST + '/commentlike/' + comment.id +'/', {
                user: this.user_id,
                like: like,
            }, {
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType: 'json',
                withCredentials: true
            })
                .then(response => {
                    // this.question = response.data;
                })
                .catch(response => {
                    console.log(error.response.data)
                })
        },

    }
})