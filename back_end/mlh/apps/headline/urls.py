from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^headlinenews/$', views.HeadLineViewSet.as_view({"get": "list"})),
    url(r'^headlinenews/(?P<pk>\d+)/$', views.HeadLineViewSet.as_view({"get": "retrieve"})),
    url(r'^newscomment/(?P<news_id>\d+)/$', views.NewsCommentView.as_view()),
    url(r'^submitnewscomment/$', views.SubmitNewsCommentView.as_view()),
    url(r'^submitnews/$', views.SubmitNewsView.as_view()),
]
