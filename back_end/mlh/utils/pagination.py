# coding = utf-8
# Version:python3.5.1
# Tools:Pycharm 2017.2.3

from rest_framework.pagination import PageNumberPagination

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 2
    page_size_query_param = 'pagi_size'
    max_page_size = 20
