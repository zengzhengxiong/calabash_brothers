from django.contrib import admin

from .models import SpitsModel, CommentModel

# Register your models here.
admin.site.register(SpitsModel)
admin.site.register(CommentModel)
