const HOST = "http://127.0.0.1:8000";

$(".activity-head").load("./header.html");
$(".footer").load("./footer.html");


// 获取url路径参数
function get_query_string(name, def) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return decodeURI(r[2]);
    }
    return def;
};

//过滤器
//字符串转日期
//获取日期的年月日
//获取日期的时分秒
//获取日期的周几
Vue.filter('str2date', function (value) {
    return new Date(value)
});

Vue.filter('str2ymd', function (value) {
    return value.getFullYear() + "-" + (value.getMonth() + 1) + "-" + value.getDate()
});

Vue.filter('str2hm', function (value) {
    return value.getHours() + ":" + (value.getMinutes())

});

const weeks = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
Vue.filter('str2week', function (value) {
    return weeks[value.getDay()]
});