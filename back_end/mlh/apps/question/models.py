from django.db import models
from users.models import User


class BaseModel(models.Model):
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')

    class Meta:
        abstract = True


class Question(BaseModel):
    author = models.ForeignKey(User, related_name='questions', on_delete=models.CASCADE, verbose_name='作者')
    title = models.CharField(max_length=64, verbose_name='问题标题')
    content = models.TextField(max_length=3000, verbose_name='问题内容')
    like_count = models.IntegerField(default=0, verbose_name='点赞数')
    click_count = models.IntegerField(default=0, verbose_name='点击量')
    answer_count = models.IntegerField(default=0, verbose_name='答案数')

    class Meta:
        db_table = 'tb_question'
        verbose_name = '问题表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title


class Answer(BaseModel):
    author = models.ForeignKey(User, related_name='answers', on_delete=models.CASCADE, verbose_name='作者')
    question = models.ForeignKey(Question, related_name='answers', on_delete=models.CASCADE, verbose_name='问题')
    content = models.CharField(max_length=3000, verbose_name='回答内容')
    like_count = models.IntegerField(default=0, verbose_name='点赞数')

    class Meta:
        db_table = 'tb_answer'
        verbose_name = '回答表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.content


class Label(BaseModel):
    name = models.CharField(max_length=32, verbose_name='标签名称')
    question = models.ManyToManyField(to='Question', related_name='labels', db_table='tb_label_question')

    class Meta:
        db_table = 'tb_label'
        verbose_name = '标签表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name
