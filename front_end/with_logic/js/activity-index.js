/**
 * Created by python on 19-1-10.
 */


// http://{{localhost}}/activitys/

var vm = new Vue({
    el: "#vm",
    data: {
        // 页面中需要使用到的数据，键值对
        host: HOST,
        activitys: '',
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,

    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
    },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
});

var app = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        host: HOST,
        activitys: '',
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        next_url:'',
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
        axios.get(HOST + "/activitys/")
            .then(response => {
                this.activitys = response.data
            })
            .catch(error => {
            });
    },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        get_next:function () {
            // var re = /^\/(\w+).html$/;
            // console.log(document.location.pathname);
            // console.log(document.location.pathname.match(re));
            this.next_url = document.location.pathname;
            console.log(this.next_url)

        },
    }
});


