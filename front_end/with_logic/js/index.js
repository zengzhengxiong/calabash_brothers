var app = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        activitys: [],
        headlinenews: [],
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        new_id: '',
        category_id: '0',
        questions: '',
        categorys:['热门','牛人专区','机器学习','后端开发','人工智能','虚拟现实','商业预测','前端开发'],
        current_category:'',

    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
        var category = this.get_query_string('category_id');
        this.current_category = category;
        // alert(category)
        if( category !== '0'&& category != null){
            // alert(this.category_id);
            axios.get(HOST + "/headlinenews/?category_id=" + category)
            .then(response => {
                this.headlinenews = response.data;
                // this.current_category = response.data.category_id;
            })
            .catch(error => {
            })
        }else {

            axios.get(HOST + "/headlinenews")
            .then(response => {
                this.headlinenews = response.data
                this.current_category = '热门'
            })
            .catch(error => {
            });

        // 获取活动日历
        axios.get(HOST + "/activitys/")
            .then(response => {
                this.activitys = response.data
            })
            .catch(error => {
            });

        // 获取热门回答
        axios.get(HOST + '/question/new/', {responseType: 'json'})
            .then(response => {
                this.questions = response.data;
            })
            .catch(response => {
                console.log(error.response.data)
            })
        }




    },
    // created(){
    //     this.headline_category()
    // },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        // news_detail: function(news_id){
        //     this.new_id = news_id
        //     axios.get(HOST + "/headlinenews/" + this.new_id +'/', {
        //             responseType: 'json'
        //         })
        //         .then(response => {
        //             location.href = 'detail.html' +'?news_id=' +this.new_id
        //         })
        //         .catch(error => {
        //             console.log(error.response.data);
        //         })
        // },
        detail_func: function (news_id) {
            location.href = 'detail.html' + '?news_id=' + news_id
        },
        activity_func: function () {
            location.href = 'activity-index.html'
        },
        question_func: function () {
            location.href = 'qa-login.html'

        },
        headline_category: function(category_id){
            // location.href = '?category_id=' + category_id;
            this.category_id = this.get_query_string('category_id');

            axios.get(HOST + "/headlinenews/?category_id=" + category_id , {
                    responseType: 'json'
                })
                .then(response => {
                    this.headlinenews = response.data;
                    // this.current_category = category_id
                    // console.log(response.data);
                    // location.href = '?category_id=' + category_id

                })
                .catch(error => {
                    // console.log(error.response.data);
                })
        },
        switch_category:function (category) {
            this.current_category = category
        },
        get_query_string: function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },

    }
})

//获取活动日历
// var bpp = new Vue({
//     el: "#bpp",
//     data: {
//         // 页面中需要使用到的数据，键值对
//         activitys: '',
//     },
//     computed: {
//         // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
//     },
//     mounted: function () {
//         // 一加载就需要做的，直接是代码
//         axios.get(HOST + "/activitys/")
//             .then(response => {
//                 this.activitys = response.data
//             })
//             .catch(error => {
//             })
//     },
//     methods: {
//         // 需要用到的函数，键值对 ，键是名称，值是匿名函数
//     }
// });