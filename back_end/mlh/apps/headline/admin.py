from django.contrib import admin

# Register your models here.
from .models import HeadLineNews, NewsComment

admin.site.register(HeadLineNews)
admin.site.register(NewsComment)
