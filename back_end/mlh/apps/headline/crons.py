# coding = utf-8
# Version:python3.5.1
# Tools:Pycharm 2017.2.3
import time

import os
from django.conf import settings
from django.template import loader

from users.models import User
from .models import HeadLineNews


def generate_static_index_html(category_id):
    """
    生成静态的主页html文件
    :return:
    """
    print('%s: generate_static_index_html' % time.ctime())
    user = User.objects.all()
    headlinenews_queryset = HeadLineNews.objects.filter(is_delete=False,category_id=category_id).select_related('user').order_by("-create_time")

    headlinenews_list = []
    for news in headlinenews_queryset:
        headlinenews_list.append({
            'id':news.id,
            'user':news.user,
            'category_id':news.category_id,
            'image':news.image,
            'title':news.title,
            'create_time':news.create_time,
            'abstract':news.abstract,
        })

    # 渲染模板
    context = {
        'headlinenews':headlinenews_list
    }
    template = loader.get_template('index.html')
    html_text = template.render(context)
    file_path = os.path.join(settings.GENERATED_STATIC_HTML_FILES_DIR, 'index.html')
    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(html_text)