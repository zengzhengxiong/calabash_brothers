from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class User(AbstractUser):
    """用户模型类"""
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')

    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username

class UserDetail(models.Model):
    """用户详情类"""
    GENDER_CHOICES = (
        (0, '男'),
        (1, '女')
    )
    user = models.ForeignKey(User, verbose_name='用户')
    real_name = models.CharField(max_length=64, verbose_name="真人姓名")
    per_mobile = models.CharField(max_length=11, verbose_name="手机号码")
    gender = models.SmallIntegerField(choices=GENDER_CHOICES, default=0, verbose_name='性别')
    email = models.CharField(max_length=32, verbose_name="个人邮箱" , blank=True, null=True)
    photo = models.CharField(max_length=128, verbose_name="真人头像", blank=True, null=True)
    birthday = models.DateField(verbose_name="出生日期")
    city = models.CharField(max_length=32, verbose_name="现居城市")
    address = models.CharField(max_length=64, verbose_name="通讯地址")
    school = models.CharField(max_length=64, verbose_name="毕业院校")
    company = models.CharField(max_length=64, verbose_name="所在公司")
    website = models.CharField(max_length=64, verbose_name="个人网站")
    abstract = models.TextField(verbose_name="个人简介")
    technology = models.TextField(verbose_name="擅长技术" , blank=True, null=True)
    work = models.TextField(verbose_name="工作经历" , blank=True, null=True)
    education = models.TextField(verbose_name="教育经历" , blank=True, null=True)



    class Meta:
        db_table = 'tb_user_detail'
        verbose_name = '用户详情'
        verbose_name_plural = verbose_name

    # def __str__(self):
    #     return self.user