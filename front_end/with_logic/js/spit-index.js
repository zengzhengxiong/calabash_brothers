//获取后端数据 axios


//展示后端数据 vue


var aaa = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        spits: [],
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        new_id: '',
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
        axios.get(HOST + "/spits/")
            .then(response => {
                //console.log(response)
                this.spits = response.data

            })
            .catch(error => {
            })
    },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数

        toggle_like: function (spit) {
            var like = spit.like += 1
            axios.put(HOST + '/spitlike/' + spit.id + '/', {
                user: this.user_id,
                like: like,
            }, {
                headers:{
                    'Authorization':'JWT '+ this.token
            },
                responseType: 'json',
                withCredentials: true})
                .then(response => {
                    // this.question = response.data;
                })
                .catch(response => {
                    console.log(error.response.data)
                })
        },

    }
})