from django.shortcuts import render
from .models import ActivityModels

# Create your views here.
from rest_framework.viewsets import ReadOnlyModelViewSet
from .serializers import LisrtModelSerializer, retrieveModelSgerializer


class ActivitysViewSet(ReadOnlyModelViewSet):
    queryset = ActivityModels.objects.all().order_by('-start_time')

    def get_serializer_class(self):
        if self.action == 'list':
            return LisrtModelSerializer
        elif self.action == 'retrieve':
            return retrieveModelSgerializer
