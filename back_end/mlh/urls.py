"""mlh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:<RegexURLPattern None ^usernames/(?P<username>\w{5,20})/count/$>
<RegexURLPattern None ^mobiles/(?P<mobile>1[3-9]\d{9})/count/$>
<RegexURLPattern None ^users/$>
<RegexURLPattern None ^authorizations/$>

Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include('users.urls')),
    url(r'',include('verifications.urls')),
    url(r'', include('activity.urls')),
    url(r'', include('spits.urls')),
    url(r'',include('headline.urls')),
    url(r'',include('question.urls')),
    # url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]

