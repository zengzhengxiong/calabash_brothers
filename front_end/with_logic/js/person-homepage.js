/**
 * Created by python on 19-1-16.
 */
var app = new Vue({
    el: '#app',
    data: {
        HOST,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        questions: [],
        usersdetail:"",

    },
    mounted: function () {
        axios.get(this.HOST + '/question/new/', {responseType: 'json'})
            .then(response => {
                this.questions = response.data;
            })
            .catch(response => {
                console.log(error.response.data)
            });
         axios.get(this.HOST + '/usersdetail/?user_id=' + this.user_id)
            .then(response => {
                this.usersdetail = response.data[0];
                // alert(this.usersdetail.real_name)
                // console.log(this.usersdetail)
            })
            .catch(response => {
                console.log(error.response.data)
            })
    },
    methods: {

    }
});
