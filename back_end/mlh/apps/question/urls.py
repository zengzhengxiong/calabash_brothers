from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^question/new/$', views.QuestionListNewView.as_view()),
    url(r'^question/hot/$', views.QuestionListHotView.as_view()),
    url(r'^question/wait/$', views.QuestionListWaitView.as_view()),
    url(r'^question/$', views.QuestionCreateView.as_view()),
    url(r'^question/(?P<pk>\d+)/$', views.QuestionDetailView.as_view()),
    url(r'^question/update/(?P<question_id>\d+)/$', views.QuestionUpdateView.as_view()),
    url(r'^answer/(?P<question_id>\d+)/$', views.AnswerView.as_view()),
    url(r'^answer/update/(?P<answer_id>\d+)/$', views.AnswerUpdateView.as_view()),
]
