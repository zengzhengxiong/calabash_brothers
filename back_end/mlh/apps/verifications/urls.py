# coding = utf-8
# Version:python3.5.1
# Tools:Pycharm 2017.2.3
from django.conf.urls import url

from . import views

urlpatterns = [

    url(r'^image_codes/(?P<image_code_id>[\w-]+)/$',views.ImageCodeView.as_view()),
    url(r'^sms_codes/(?P<mobile>1[3-9]\d{9})/$',views.SMSCodeView.as_view()),
]

