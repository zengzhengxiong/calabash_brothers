from django.db import models


# Create your models here.

class ActivityModels(models.Model):
    STATUS_CHOICE = ((1, "立即报名"), (2, "报名截止"), (3, "活动已结束"))

    image = models.CharField(max_length=128)
    title = models.CharField(max_length=32)
    start_time = models.DateTimeField(verbose_name='开始时间')
    end_time = models.DateTimeField(verbose_name='结束时间')
    apply_time = models.DateTimeField(verbose_name='报名截止时间')
    status = models.SmallIntegerField(choices='', verbose_name='状态')
    city = models.CharField(max_length=64, verbose_name='举办城市')
    address = models.CharField(max_length=128, verbose_name='地址')
    host = models.CharField(max_length=64, verbose_name='主办方')
    abstract = models.TextField(verbose_name='简介')
    description = models.TextField(verbose_name='详情')
    link = models.CharField(max_length=128, verbose_name='链接')

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'tb_activity'
        verbose_name = '活动'
        verbose_name_plural = verbose_name
