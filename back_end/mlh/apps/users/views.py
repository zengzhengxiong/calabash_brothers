from django.shortcuts import render

# Create your views here.
from rest_framework.generics import CreateAPIView,ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import User,UserDetail
from . import serializers


class UserView(CreateAPIView):
    """
    用户注册
    """
    serializer_class = serializers.CreateUserSerializer


class UsernameCountView(APIView):
    """
    判断用户名是否存在
    """

    def get(self, request, username):
        """获取用户名的数量"""
        count = User.objects.filter(username=username).count()

        data = {
            'username': username,
            'count': count
        }

        return Response(data)


class MobileCountView(APIView):
    """
    判断手机号是否存在
    """

    def get(self, request, mobile):
        count = User.objects.filter(mobile=mobile).count()

        data = {
            'mobile': mobile,
            'count': count,
        }
        return Response(data)


class UserDetailView(ListAPIView):
    """用户详细资料"""
    def get_queryset(self):
        user_id = self.request.query_params.get('user_id')
        print(user_id)
        queryset = UserDetail.objects.filter(user=user_id)
        print(queryset)
        return queryset

    serializer_class = serializers.UserDetailSerializer