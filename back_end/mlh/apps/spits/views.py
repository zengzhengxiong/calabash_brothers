
from rest_framework.generics import GenericAPIView, ListAPIView, CreateAPIView, UpdateAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework.viewsets import ReadOnlyModelViewSet


from spits.models import SpitsModel, CommentModel


from spits.serializers import CommentSerializer, SpitsListSerializer, SpitsDetailSerializer, SpitsubmitSerializer, \
    SpitlikeSerializer,CommentlikeSerializer



class SpitsViewSet(ReadOnlyModelViewSet):
    queryset = SpitsModel.objects.filter(is_delete=False).order_by('-create_time')

    def get_serializer_class(self):
        if self.action == 'list':

            return SpitsListSerializer
        else:
            return SpitsDetailSerializer


class CommentListView(ListAPIView):
    serializer_class = CommentSerializer


    def get_queryset(self):
        spits_id = self.kwargs['spits_id']

        comment_spis = CommentModel.objects.filter(spits=spits_id)
        return comment_spis


class CommentsubmitView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CommentSerializer

class SpitsubmitView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = SpitsubmitSerializer



class SpitlikeView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = SpitlikeSerializer

    def get_object(self):
        spit_id=self.kwargs['spit_id']
        return SpitsModel.objects.get(id=spit_id)


class CommentlikeView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CommentlikeSerializer

    def get_object(self):
        comment_id = self.kwargs['comment_id']
        return CommentModel.objects.get(id=comment_id)


