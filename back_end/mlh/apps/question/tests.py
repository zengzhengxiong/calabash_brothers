from django.test import TestCase

# Create your tests here.

from question.models import Question, Answer, Label
from users.models import User

# label_name = ['python', 'java', 'php', 'js', 'html']
#
# for name in label_name:
#     Label.objects.create(name=name)

for i in range(100):
    question = Question.objects.create(author_id=i % 3 + 1, title='问题标题' + str(i), content='问题内容' + str(i))
    question.labels.add(*list(
        Label.objects.filter(id__in=(i % 5 + 1, i * 2 % 5 + 1))
    ))


for i in range(100):
    answer = Answer.objects.create(author_id=i %3+1, content='问题答案' + str(i), question_id=i+1)

import random

for i in range(100):
    Question.objects.update(id=i, answer_count = random.randint(1,20))
