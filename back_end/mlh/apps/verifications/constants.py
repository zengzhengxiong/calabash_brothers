# coding = utf-8
# Version:python3.5.1
# Tools:Pycharm 2017.2.3

# 图片验证码保存时长 单位:s
IMAGE_CODE_REDIS_EXPIRES = 5 * 60

# 短信验证码保存时长 单位s
SMS_CODE_REDIS_EXPIRES = 5 * 60

# 发送间隔 单位s
SEND_SMS_CODE_INTERVAL = 60

# 模板代码编号
SMS_CODE_TEMP_ID = 1
