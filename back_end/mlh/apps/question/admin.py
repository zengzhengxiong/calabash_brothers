from django.contrib import admin
from .models import Question, Answer, Label
# Register your models here.

admin.site.register(Question)
admin.site.register( Label)
admin.site.register(Answer)



from rest_framework.viewsets import ReadOnlyModelViewSet