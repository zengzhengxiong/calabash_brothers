var aaa = new Vue({
    el:"#app",
    data:{
        // 页面中需要使用到的数据，键值对
        categorys:['机器学习','后端开发','人工智能','虚拟现实','商业预测','前端开发'],
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        category_id: '',
        current_category:'',
        default_category:'牛人专区',
        news_title:"",
        submit_content : ""

    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        CKEDITOR.replace('editor1');
        this.current_category = 0;
    },
    methods:{
        getNowFormatDate: function () {
            var date = new Date();
            var seperator1 = "-";
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var strDate = date.getDate();
            var HourDate = date.getHours();
            var MinutesDate = date.getMinutes();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            var currentdate = year + seperator1 + month + seperator1 + strDate + ' ' + HourDate + ':' + MinutesDate;
            return currentdate;
        },
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        submit_news:function () {
            // alert(11);
            // alert(this.current_category + 1);
            // console.log(CKEDITOR.instances.editor1.getData());
            // this.submit_content = CKEDITOR.instances.editor1.getData()
        // alert (this.submit_content);

            axios.post(HOST + '/submitnews/', {
                user: this.user_id,
                category_id: this.current_category + 1,
                title:this.news_title,
                content: CKEDITOR.instances.editor1.getData(),
                create_time: this.getNowFormatDate(),
            }, {
                headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                    responseType: 'json',
                    withCredentials: true
            })
            .then(response => {
                alert('发布成功');
                location.href = '/';
            })
            .catch(error => {
                alert('发布失败')
                var status = error.response.status;
                if (status == 401 || status == 403) {
                    location.href = 'person-loginsign.html?next=/headline_submit.html' ;
                } else {
                    console.log(error.response.data);
                }
            })
        }
    }
})