from rest_framework import serializers

from spits.models import SpitsModel,CommentModel
from users.models import User


class SpitsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpitsModel
        fields = ('id','title','create_time','reply','like')



class SpitsDetailSerializer(serializers.ModelSerializer):

    username = serializers.SerializerMethodField(label='作者')
    comments = serializers.SerializerMethodField(label='评论数')

    class Meta:
        model = SpitsModel

        fields = '__all__'

    def get_username(self,obj):
        return obj.user.username

    def get_comments(self,obj):
        return obj.commentmodel_set.count()


class CommentSerializer(serializers.ModelSerializer):

    username = serializers.SerializerMethodField(label='作者')

    class Meta:
        model = CommentModel

        fields = '__all__'

    def get_username(self,obj):
        return obj.user.username


class SpitsubmitSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpitsModel
        exclude = ('image',)

class SpitlikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpitsModel
        fields = ('like',)

class CommentlikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommentModel
        fields = ('like',)