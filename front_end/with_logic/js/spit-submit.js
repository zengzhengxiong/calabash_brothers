
var aaa = new Vue({
    el:"#app",
    data:{
        // 页面中需要使用到的数据，键值对
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数

    },
    mounted:function () {
        CKEDITOR.replace('editor2');
    },
    methods: {
        spitsubmit: function () {
            axios.post(HOST + '/spitsubmit/', {
                user: this.user_id,
                title: CKEDITOR.instances.editor2.getData(),
                description:CKEDITOR.instances.editor2.getData(),
            }, {
                headers:{
                    'Authorization':'JWT '+ this.token
            },
                responseType: 'json',
                withCredentials: true
            })
                .then(response => {
                    // 记录用户的登录状态
                    alert('发布成功')
                    location.href='/spit-index.html'
                })
                .catch(error => {
                if (error.response.status == 400) {
                    if ('non_field_errors' in error.response.data) {
                        this.error_sms_code_message = error.response.data.non_field_errors[0];
                    } else {
                        this.error_sms_code_message = '数据有误';
                    }
                    this.error_sms_code = true;
                } else {
                    console.log(error.response.data);
                }
            })
        },
    },

})