var app = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        headlinenews: [],
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        news_id: "",
        news_detail: "",
        newscomment: [],
        comment_content: "",
        create_time: "",
        spits:[],
        child_comment_content:"",

    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {

        this.news_id = this.get_query_string('news_id');

        //热门推荐
        axios.get(HOST+"/headlinenews/")
        .then(response=>{
            //console.log(response)
            this.headlinenews = response.data

        })
        .catch(error =>{}),

        // 一加载就需要做的，直接是代码
        axios.get(HOST + "/headlinenews/" + this.news_id + '/', {
            responseType: 'json'
        })
            .then(response => {
                this.news_detail = response.data

            })
            .catch(error => {
                console.log(error.response.data);
            }),
            // this.news_id = this.get_query_string('news_id');

        // 一加载就需要做的，直接是代码
        axios.get(HOST + "/newscomment/" + this.news_id + '/', {
            responseType: 'json'
        })
            .then(response => {
                this.newscomment = response.data

            })
            .catch(error => {
                console.log(error.response.data);
            }),

        //热门吐槽
        axios.get(HOST+"/spits/")
        .then(response=>{
            //console.log(response)
            this.spits = response.data

        })
        .catch(error =>{})

    },

    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        // 获取url路径参数
        get_query_string: function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        getNowFormatDate: function () {
            var date = new Date();
            var seperator1 = "-";
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var strDate = date.getDate();
            var HourDate = date.getHours();
            var MinutesDate = date.getMinutes();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            var currentdate = year + seperator1 + month + seperator1 + strDate + ' ' + HourDate + ':' + MinutesDate;
            return currentdate;
        },
        submitnewscomment: function () {
            axios.post(HOST + '/submitnewscomment/', {
                user: this.user_id,
                headlinenews: this.news_id,
                content: this.comment_content,
                create_time: this.getNowFormatDate(),
            }, {
                headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                    responseType: 'json',
                    withCredentials: true
            })
            .then(response => {
                alert('评论成功')
                location.reload()
            })
            .catch(error => {
                var status = error.response.status;
                if (status == 401 || status == 403) {
                    location.href = 'person-loginsign.html?next=/detail.html?news_id=' + this.news_id ;
                } else {
                    console.log(error.response.data);
                }
            })
        },
        to_reply:function (event) {
            $(event.target).parents(".text").siblings(".edit-box").toggle();
        },
        submitchildnewscomment: function (parent_id) {
            axios.post(HOST + '/submitnewscomment/', {
                user: this.user_id,
                headlinenews: this.news_id,
                content: this.child_comment_content,
                parent: parent_id,
                create_time: this.getNowFormatDate(),
            }, {
                headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                    responseType: 'json',
                    withCredentials: true
            })
            .then(response => {
                alert('评论成功');
                location.reload()
            })
            .catch(error => {
                var status = error.response.status;
                if (status == 401 || status == 403) {
                    location.href = 'person-loginsign.html?next=/detail.html?news_id=' + this.news_id ;
                } else {
                    console.log(error.response.data);
                }
            })
        },
    },

})



