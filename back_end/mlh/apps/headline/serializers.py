from rest_framework import serializers

from users.models import User
from users.serializers import UserSerializer
from .models import HeadLineNews, NewsComment


# class HeadLineNewsSerializer(serializers.ModelSerializer):
#     author_name = serializers.SerializerMethodField(label='作者')
#
#     class Meta:
#         model = HeadLineNews
#         exclude = ('author', 'content', 'is_delete')
#
#     def get_author_name(self, obj):
#         return obj.author.username
#
#
# class HeadLineNewsDetailSerializer(serializers.ModelSerializer):
#     author_name = serializers.SerializerMethodField(label='作者')
#
#     class Meta:
#         model = HeadLineNews
#         fields = ('id', 'title', 'author', 'create_time', 'content', 'author_name')
#
#     def get_author_name(self, obj):
#         return obj.author.username
#
#
# class NewsCommentSerializer(serializers.ModelSerializer):
#     news_comment_author_name = serializers.SerializerMethodField(label='新闻评论作者')
#
#     class Meta:
#         model = NewsComment
#         fields = ("content", "news_comment_author_name")
#
#     def get_news_comment_author_name(self, obj):
#         return obj.user.username

class HeadLineNewsUserSerializer(serializers.ModelSerializer):
    """首页新闻列表序列化器"""
    user = UserSerializer()
    class Meta:
        model = HeadLineNews
        exclude = ('content', 'is_delete')


class HeadLineNewsDetailUserSerializer(serializers.ModelSerializer):
    """新闻详情序列化器"""
    user = UserSerializer()
    comment_count = serializers.SerializerMethodField(label='评论总数量')

    class Meta:
        model = HeadLineNews
        fields = ('title', 'create_time', 'content', 'user', 'comment_count')

    def get_comment_count(self, obj):
        """
        返回当前评论总数量数量
        固定写法,obj代表Role实例对象,模型类配置了反向引用user代表当前角色用户
        """
        news_id = self.context['view'].kwargs['pk']
        number = obj.newscomment_set.filter(headlinenews_id=news_id).count()
        return number


class SubmitNewsSerializer(serializers.ModelSerializer):
    """提交评论序列化器"""
    class Meta:
        model = HeadLineNews
        fields = ('title', 'create_time', 'content', 'user', 'category_id')


class NewscommentSerializer(serializers.ModelSerializer):
    """新闻评论序列化器"""
    # headlinenews = HeadLineNewsUserSerializer()
    user = UserSerializer()

    class Meta:
        model = NewsComment
        fields = ('id', 'create_time', 'content', 'headlinenews', 'user')
        # fields = '__all__'
        # depth = 1



class NewscommentNewscommentSerializer(serializers.ModelSerializer):
    newscomment_set = NewscommentSerializer(many=True)
    user = UserSerializer()
    child_comment_count = serializers.SerializerMethodField(label='子评论数量')

    class Meta:
        model = NewsComment
        fields = ('id', 'create_time', 'content', 'headlinenews', 'newscomment_set', 'user', 'child_comment_count')
        # fields = '__all__'
        # depth = 1

    def get_child_comment_count(self, obj):
        """
        返回当前评论总数量数量
        固定写法,obj代表Role实例对象,模型类配置了反向引用user代表当前角色用户
        """
        news_id = self.context['view'].kwargs['news_id']
        number = obj.newscomment_set.filter(headlinenews_id=news_id).count()
        return number


class SubmitNewsCommentSerializer(serializers.ModelSerializer):
    """提交新闻评论序列化器"""
    class Meta:
        model = NewsComment
        fields = '__all__'