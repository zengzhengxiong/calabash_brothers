from django.db import models

# Create your models here.
from mlh import settings
from users.models import User


class SpitsModel(models.Model):
    title = models.TextField(verbose_name='标题')
    create_time = models.DateTimeField(auto_now_add=True,verbose_name='发布时间',)
    is_collected = models.BooleanField(default=False,verbose_name='收藏')
    user = models.ForeignKey(User,on_delete=models.CASCADE, verbose_name='用户id')
    image =models.CharField(verbose_name='图片',max_length=128)
    description = models.TextField(verbose_name='详情',)
    like = models.IntegerField(verbose_name='点赞',default=0)
    reply = models.IntegerField(verbose_name='回复',default=0)
    is_delete = models.BooleanField(verbose_name='逻辑删除',default=False)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'tb_spitslot'
        verbose_name = '吐槽'
        verbose_name_plural = verbose_name



class CommentModel(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE, verbose_name='用户id')
    spits = models.ForeignKey(SpitsModel,on_delete=models.CASCADE, verbose_name='吐槽id')
    content = models.CharField(verbose_name='评论内容',max_length=128)
    like = models.IntegerField(verbose_name='点赞',default=0)

    class Meta:
        db_table = 'tb_comment'
        verbose_name = '评论'
        verbose_name_plural = verbose_name


