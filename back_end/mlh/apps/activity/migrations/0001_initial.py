# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-01-17 07:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ActivityModels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.CharField(max_length=128)),
                ('title', models.CharField(max_length=32)),
                ('start_time', models.DateTimeField(verbose_name='开始时间')),
                ('end_time', models.DateTimeField(verbose_name='结束时间')),
                ('apply_time', models.DateTimeField(verbose_name='报名截止时间')),
                ('status', models.SmallIntegerField(verbose_name='状态')),
                ('city', models.CharField(max_length=64, verbose_name='举办城市')),
                ('address', models.CharField(max_length=128, verbose_name='地址')),
                ('host', models.CharField(max_length=64, verbose_name='主办方')),
                ('abstract', models.TextField(verbose_name='简介')),
                ('description', models.TextField(verbose_name='详情')),
                ('link', models.CharField(max_length=128, verbose_name='链接')),
            ],
            options={
                'db_table': 'tb_activity',
                'verbose_name': '活动',
                'verbose_name_plural': '活动',
            },
        ),
    ]
