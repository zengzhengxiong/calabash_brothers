from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^activitys/$', views.ActivitysViewSet.as_view({'get': 'list'})),
    url(r'^activitys/(?P<pk>\d+)/$', views.ActivitysViewSet.as_view({'get': 'retrieve'})),
]
