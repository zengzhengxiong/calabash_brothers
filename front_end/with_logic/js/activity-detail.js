/**
 * Created by python on 19-1-10.
 */




var apps = new Vue({
    el: "#apps",
    data: {
        // 页面中需要使用到的数据，键值对
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
    },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
})

var app = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        activitys: '',
        remain_time: 0,
        deadline: 0,
        now: 0,
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
        axios.get(HOST + "/activitys/" + get_query_string('id', 1))
            .then(response => {
                this.activitys = response.data
                //开启定时器，得到数字在ｈｔｍｌ中转换成ｘ天ｘ时ｘ秒
                //倒计时　＝　截止时间　－　当前时间(setinterval)
                this.deadline = Math.floor(new Date(response.data.apply_time).getTime() / 1000);
                let server_now = response.headers.date;
                // console.log(server_now)
                // this.now = Math.floor(new Date().getTime() / 1000);
                this.now = Math.floor(new Date(server_now).getTime() / 1000);

                this.remain_time = this.deadline - this.now;

                setInterval(() => {
                    this.now += 1;
                    this.remain_time = this.deadline - this.now
                }, 1000)
            })
            .catch(error => {
                return {message: '我是你爸爸！！！'}
            })
    },

    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    },
    filters: {
        second2dhms: function (value) {
            let day = Math.floor(value / (60 * 60 * 24));
            let hour = Math.floor((value / 60 / 60)) % 24;
            let minute = Math.floor((value / 60)) % 60;
            let second = Math.floor(value) % 60;
            return day + "天" + hour + "小时" + minute + "分" + second + "秒"
        }
    }
});

var vm = new Vue({
    el: "#vm",
    data: {
        // 页面中需要使用到的数据，键值对
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        activitys: '',
        remain_time: 0,
        deadline: 0,
        now: 0,
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
        axios.get(HOST + "/activitys/" + get_query_string('id', 1))
            .then(response => {
                this.activitys = response.data
                //开启定时器，得到数字在ｈｔｍｌ中转换成ｘ天ｘ时ｘ秒
                //倒计时　＝　截止时间　－　当前时间(setinterval)
                this.deadline = Math.floor(new Date(response.data.apply_time).getTime() / 1000);
                let server_now = response.headers.date;
                // console.log(server_now)
                // this.now = Math.floor(new Date().getTime() / 1000);
                this.now = Math.floor(new Date(server_now).getTime() / 1000);

                this.remain_time = this.deadline - this.now;

                setInterval(() => {
                    this.now += 1;
                    this.remain_time = this.deadline - this.now
                }, 1000)
            })
            .catch(error => {
                return {message: '我是你爸爸！！！'}
            })
    },

    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    },
    filters: {
        second2dhms: function (value) {
            let day = Math.floor(value / (60 * 60 * 24));
            let hour = Math.floor((value / 60 / 60)) % 24;
            let minute = Math.floor((value / 60)) % 60;
            let second = Math.floor(value) % 60;
            return day + "天" + hour + "小时" + minute + "分" + second + "秒"
        }
    }
});