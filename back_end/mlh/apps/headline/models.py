from django.db import models

# Create your models here.
from users.models import User


# class HeadLineNews(models.Model):
#     """头条首页模型类"""
#     category_id = models.CharField(verbose_name="分类", max_length=20)
#     image = models.CharField(verbose_name="图片", max_length=100)
#     title = models.CharField(verbose_name="标题", max_length=30)
#     create_time = models.DateTimeField(verbose_name="发布时间")
#     abstract = models.TextField(verbose_name="摘要", )
#     is_delete = models.BooleanField(verbose_name="是否删除", default=False)
#     content = models.TextField(verbose_name="新闻正文")
#     author = models.ForeignKey(User, verbose_name="作者名字", on_delete=models.CASCADE, default=1, related_name="author")
#
#     def __str__(self):
#         return self.title
#
#     class Meta:
#         db_table = "headlinenews"
#         verbose_name = "头条新闻"
#         verbose_name_plural = verbose_name

class HeadLineNews(models.Model):
    """头条首页模型类"""
    category_id = models.CharField(verbose_name="分类", max_length=20)
    image = models.CharField(verbose_name="图片", max_length=100)
    title = models.CharField(verbose_name="标题", max_length=30)
    create_time = models.DateTimeField(verbose_name="发布时间")
    abstract = models.TextField(verbose_name="摘要", )
    is_delete = models.BooleanField(verbose_name="是否删除", default=False)
    content = models.TextField(verbose_name="新闻正文")
    user = models.ForeignKey("users.User", blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = "headlinenews"
        verbose_name = "头条新闻"
        verbose_name_plural = verbose_name


# class NewsComment(models.Model):
#     parent = models.ForeignKey("self", verbose_name="父评论", on_delete=models.SET_NULL, blank=True, null=True,
#                                related_name="newscomment")
#     user = models.ForeignKey(User, verbose_name="所属用户", on_delete=models.CASCADE, related_name="newscomment")
#     news = models.ForeignKey(HeadLineNews, verbose_name="所属新闻", on_delete=models.CASCADE, related_name="newscomment")
#     content = models.CharField(verbose_name="评论内容", max_length=100)
#     create_time = models.DateTimeField(verbose_name="评论的时间")
#
#     def __str__(self):
#         return self.content
#
#     class Meta:
#         db_table = "comment"
#         verbose_name = "新闻评论"
#         verbose_name_plural = verbose_name

class NewsComment(models.Model):
    parent = models.ForeignKey("self", verbose_name="父评论", on_delete=models.SET_NULL, blank=True, null=True)
    user = models.ForeignKey("users.User", blank=True, null=True)
    headlinenews = models.ForeignKey("HeadLineNews")
    content = models.CharField(verbose_name="评论内容", max_length=100)
    create_time = models.DateTimeField(verbose_name="评论的时间")

    def __str__(self):
        return self.content

    class Meta:
        db_table = "comment"
        verbose_name = "新闻评论"
        verbose_name_plural = verbose_name


