import logging
import random

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from celery_tasks.sms.tasks import send_sms_code
from rest_framework.views import APIView

from libs.captcha.captcha import captcha
from mlh.utils.yuntongxun.sms import CCP
from . import serializers
from . import constants

logger = logging.getLogger('django')

class ImageCodeView(APIView):
    """
    图片验证码
    """
    def get(self,request,image_code_id):
        #　生成验证码图片
        text, image = captcha.generate_captcha()
        print("\033[1;33;45m 图片验证码:%s \033[0m" % text)

        # redis保存验证码图片
        redis_conn = get_redis_connection('verify_codes')
        redis_conn.setex('img_%s'%image_code_id,constants.IMAGE_CODE_REDIS_EXPIRES,text)

        # 返回结果
        return HttpResponse(image,content_type='image/jpg')

class SMSCodeView(GenericAPIView):
    """
    短信验证码
    """
    serializer_class = serializers.ImageCodeCheckSerializer

    def get(self, request, mobile):
        """
        创建短信验证码
        """
        # 判断图片验证码, 判断是否在60s内
        serializer = self.get_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        # 生成短信验证码
        sms_code = "%06d" % random.randint(0, 999999)
        print("\033[1;31;47m 短信验证码:%s \033[0m" % sms_code)

        # 保存短信验证码与发送记录
        redis_conn = get_redis_connection('verify_codes')
        pl = redis_conn.pipeline()
        pl.setex("sms_%s" % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        pl.setex("send_flag_%s" % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)
        pl.execute()

        # 发送短信验证码

        # try:
        #     result = CCP().send_template_sms(mobile,[sms_code,str(constants.SMS_CODE_REDIS_EXPIRES//60)],constants.SMS_CODE_TEMP_ID)
        #     # result = 0
        # except Exception as e:
        #     logger.error("发送验证码短信[异常][ mobile: %s, message: %s ]" % (mobile, e))
        #     return Response({'message':'failed'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        # else:
        #     if result == 0:
        #         logger.info("发送验证码短信[正常][ mobile: %s ]" % mobile)
        #         return Response({'message': 'OK'})
        #     else:
        #         logger.warning("发送验证码短信[失败][ mobile: %s ]" % mobile)
        #         return Response({'message': 'failed'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # 使用celery发送短信验证码
        #
        # sms_code_expires = str(constants.SMS_CODE_REDIS_EXPIRES // 60)
        # send_sms_code.delay(mobile, sms_code, sms_code_expires,constants.SMS_CODE_TEMP_ID)
        #
        return Response({"message": "OK"})


