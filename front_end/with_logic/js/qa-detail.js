var vm = new Vue({
    el: '#app',
    data: {
        HOST,
        question: '',
        question_id: '',
        answer_list: [],
        answer_content:'',
        error_content: false,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
    },
    mounted: function () {
        this.question_id = this.get_query_string('question_id');

        axios.get(this.HOST + '/question/' + this.question_id + '/', {responseType: 'json'})
            .then(response => {
                this.question = response.data;
            })
            .catch(response => {
                console.log(error.response.data)
            });
        axios.get(this.HOST + '/answer/' + this.question_id + '/', {responseType: 'json'})
            .then(response => {
                this.answer_list = response.data;
            })
            .catch(response => {
                console.log(error.response.data)
            });
    },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        // 获取url路径参数
        get_query_string: function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        question_up: function () {
            var like_count = this.question.like_count += 1;
            axios.put(this.HOST + '/question/update/' + this.question_id + '/', {
                like_count: like_count,
            }, {responseType: 'json'})
                .then(response => {
                    // this.question = response.data;
                })
                .catch(response => {
                    console.log(error.response.data)
                })
        },
        question_down: function () {
            var like_count = this.question.like_count -= 1;
            axios.put(this.HOST + '/question/update/' + this.question_id + '/', {
                like_count: like_count,
            }, {responseType: 'json'})
                .then(response => {
                    // this.question = response.data;
                })
                .catch(response => {
                    console.log(error.response.data)
                })
        },
        answer_up: function (answer) {
            var like_count = answer.like_count += 1;
            axios.put(this.HOST + '/answer/update/' + answer.id + '/', {
                like_count: like_count,
            }, {responseType: 'json'})
                .then(response => {
                })
                .catch(response => {
                    console.log(error.response.data)
                })
        },
        answer_down: function (answer) {
            var like_count = answer.like_count -= 1;
            axios.put(this.HOST + '/answer/update/' + answer.id + '/', {
                like_count: like_count,
            }, {responseType: 'json'})
                .then(response => {
                })
                .catch(response => {
                    console.log(error.response.data)
                })
        },
        // 检查数据
        check_content: function () {
            if (!this.content) {
                this.error_content = true;
            } else {
                this.error_content = false;
            }
        },
        // 表单提交
        on_submit: function () {
            this.check_content();

            if (this.error_content == false) {
                axios.post(this.HOST + '/answer/', {
                        author:this.user_id,
                        content: this.answer_content,
                    },
                    // {
                    //     headers:{
                    //         'Authorization':'JWT '+this.token
                    //     }
                    // },
                    {
                        responseType: 'json',
                    })
                    .then(response => {

                        // 跳转页面
                        var return_url = this.get_query_string('next');
                        if (!return_url) {
                            return_url = '/qa-detail.html';
                        }
                        location.href = return_url;
                    })
                    .catch(error => {
                        console.log(error.response.data)
                    })
            }
        },

    }
});
