var vm = new Vue({
    el: '#app',
    data: {
        HOST,
        questions: [],
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,

    },
    mounted: function () {
        axios.get(this.HOST + '/question/new/', {responseType: 'json'})
            .then(response => {
                this.questions = response.data;
            })
            .catch(response => {
                console.log(error.response.data)
            })
    },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        // 获取url路径参数
        get_query_string: function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        new_question:function () {
            axios.get(this.HOST + '/question/new/', {responseType: 'json'})
            .then(response => {
                this.questions = response.data;
            })
            .catch(response => {
                console.log(error.response.data)
            })
        },
        hot_question:function () {
            axios.get(this.HOST + '/question/hot/', {responseType: 'json'})
            .then(response => {
                this.questions = response.data;
            })
            .catch(response => {
                console.log(error.response.data)
            })
        },
        wait_question:function () {
            axios.get(this.HOST + '/question/wait/', {responseType: 'json'})
            .then(response => {
                this.questions = response.data;
            })
            .catch(response => {
                console.log(error.response.data)
            })
        },
    }
});
