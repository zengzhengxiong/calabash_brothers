from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token

from . import views

urlpatterns = [
    url('^spits/$', views.SpitsViewSet.as_view({"get": "list"})),
    url('^spits/(?P<pk>\d+)$', views.SpitsViewSet.as_view({"get": "retrieve"})),
    url('^comments/(?P<spits_id>\d+)$', views.CommentListView.as_view()),
    url('^commentsubmit/$',views.CommentsubmitView.as_view()),
    url('^spitsubmit/$',views.SpitsubmitView.as_view()),
    url('^spitlike/(?P<spit_id>\d+)/$',views.SpitlikeView.as_view()),
    url('^commentlike/(?P<comment_id>\d+)/$',views.CommentlikeView.as_view()),
]
