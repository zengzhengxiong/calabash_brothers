# coding = utf-8
# Version:python3.5.1
# Tools:Pycharm 2017.2.3
from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token

from . import views

urlpatterns = [
    url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UsernameCountView.as_view()),
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
    url(r'^users/$', views.UserView.as_view()),
    url(r'^usersdetail/$', views.UserDetailView.as_view()),
    url(r'^authorizations/$', obtain_jwt_token),  # 登录认证
]

# print(*urlpatterns, sep='\n')
